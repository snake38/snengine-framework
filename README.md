SnEngine Framework
------------------

Simple framework for 2D games, written in C++ using OpenGL 3. Written for a little 24h compo. Well documented. Can load images (for now image must have channel alpha) for texturing, sprites with animations (load image with sprites, set one frame size and start animation with simple API function calls), simple sound and input handling. 

Simple example:

```cpp
#include <SnEngine.h>

using namespace se;

int main(int argc, char* argv[])
{
	Window* window = new Window(640, 480);
	
	Timer timer;
	float dt;
	
	Sprite* sprite = new Sprite(new Image("logo.png"));
	sprite->SetScale(0.5f, 0.5f);
	
	while (!window->ShouldClose())
	{
		dt = static_cast<float>(timer.Restart());
		
		window->Clear(Color());
		
		window->Draw(sprite);
		
		window->Render();
	}
	
	window->shutdown();

	return 0;
}

```