#ifndef SNENGINE_TIMER_H
#define SNENGINE_TIMER_H

#include "GLFW/glfw3.h"

namespace se
{
	class Timer
	{
	private:
		double m_secondsElapsed;
		double m_firstTimeClock;
	public:
		/**
		 *	\brief Creates Timer and starts counting
		 */
		Timer();

		/**
		 * \brief Returns elapsed time in seconds
		 */
		double GetTime();

		/**
		 * \brief Returns elapsed time and restart counting
		 */
		double Restart();
	};
}

#endif