#include "Window.h"

namespace se
{
	void err_cb(int err, const char* str)
	{
		printf("%d: %s\n", err, str);
	}

	Window::Window(int width, int height, bool fullscreen)
	{
		glfwSetErrorCallback(err_cb);

		if(!glfwInit())
			return;

		glfwWindowHint(GLFW_SAMPLES, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

		m_window = glfwCreateWindow(width, height, "", NULL, NULL);
		if(!m_window)
		{
			glfwTerminate();
			return;
		}

		glfwMakeContextCurrent(m_window);
		glfwSwapInterval(1);
		
		glewExperimental = GL_TRUE;
		GLenum err = glewInit();
		if(err != GLEW_OK)
		{
			printf("glewInit(): %s\n", glewGetErrorString(err));
			glfwTerminate();
			return;
		}

		glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);		

		//glm::mat4 projectionMatrix = glm::ortho(0.f, float(width), float(height), 0.f);

		GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
		GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

		static const char* vertexShaderSource = 
			"#version 150										\n\
																\n\
			in vec3 vertexPos;									\n\
			uniform mat4 MP;									\n\
																\n\
			in vec2 vertexUV;									\n\
			in vec4 blendingColor;								\n\
			out vec2 UV;										\n\
			out vec4 blend;										\n\
																\n\
			void main()											\n\
			{													\n\
				gl_Position = MP * vec4(vertexPos, 1);			\n\
																\n\
				UV = vertexUV;									\n\
				blend = blendingColor;							\n\
			}													\n\
			";
		static const char* fragmentShaderSource = 
			"#version 150										\n\
																\n\
			in vec2 UV;											\n\
			in vec4 blend;										\n\
																\n\
			out vec4 color;										\n\
																\n\
			uniform sampler2D textureSampler;					\n\
																\n\
			void main()											\n\
			{													\n\
			color = texture(textureSampler, UV) * blend;		\n\
			}													\n\
			";
		
		/*int result = 0, logLen;
		char *log = (char*)malloc(1024);*/

		glShaderSource(vertexShaderID, 1, &vertexShaderSource, NULL);
		glCompileShader(vertexShaderID);

		/*glGetShaderiv(vertexShaderID, GL_COMPILE_STATUS, &result);
		glGetShaderiv(vertexShaderID, GL_INFO_LOG_LENGTH, &logLen);
		glGetShaderInfoLog(vertexShaderID, logLen, NULL, log);
		printf("Vertex Log: %s\n", log);*/

		glShaderSource(fragmentShaderID, 1, &fragmentShaderSource, NULL);
		glCompileShader(fragmentShaderID);

		/*glGetShaderiv(fragmentShaderID, GL_COMPILE_STATUS, &result);
		glGetShaderiv(fragmentShaderID, GL_INFO_LOG_LENGTH, &logLen);
		glGetShaderInfoLog(fragmentShaderID, logLen, NULL, log);
		printf("Fragment Log: %s\n", log);*/

		m_shaderSprite = glCreateProgram();
		glAttachShader(m_shaderSprite, vertexShaderID);
		glAttachShader(m_shaderSprite, fragmentShaderID);
		glLinkProgram(m_shaderSprite);

		glDeleteShader(vertexShaderID);
		glDeleteShader(fragmentShaderID);

		/*glGetProgramiv(m_shaderSprite, GL_LINK_STATUS, &result);
		glGetProgramiv(m_shaderSprite, GL_INFO_LOG_LENGTH, &logLen);
		glGetProgramInfoLog(m_shaderSprite, logLen, NULL, log);
		printf("Program Log: %s\n", log);*/


		m_width = float(width);
		m_height = float(height);

		AudioManager::getInstance().initialize(width, height);
	}

	Window::~Window()
	{
		AudioManager::getInstance().finalize();
	}

	void Window::Draw(Drawable* object)
	{
		object->draw(m_shaderSprite, m_width, m_height);
	}

	bool Window::GetKey(Input::KEYS key)
	{
		if(glfwGetKey(m_window, key) == GLFW_PRESS)
			return true;
		else
			return false;
	}

	Vec2i Window::GetMousePos()
	{
		double xpos, ypos;
		glfwGetCursorPos(m_window, &xpos, &ypos);
		int x = int(floor(xpos));
		int y = int(floor(ypos));

		return Vec2<int>(x, y);
	}

	bool Window::GetMouseButton(Input::MOUSE button)
	{
		if(glfwGetMouseButton(m_window, button) == GLFW_PRESS)
			return true;
		else
			return false;
	}

	void Window::Render()
	{
		glfwSwapBuffers(m_window);
		glfwPollEvents();
	}

	bool Window::ShouldClose()
	{
		if(glfwWindowShouldClose(m_window) == 0)
			return false;
		else
			return true;
	}

	void Window::Clear(Color color)
	{
		glClearColor(float(color.r) / 255.f, float(color.g) / 255.f, float(color.b) / 255.f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);
	}

	void Window::Close()
	{
		glfwSetWindowShouldClose(m_window, true);
	}

	void Window::SetTitle(std::string title)
	{
		glfwSetWindowTitle(m_window, title.c_str());
	}

	void Window::Shutdown()
	{
		glfwTerminate();
	}

	Vec2i Window::Size()
	{
		return Vec2<int>(int(m_width), int(m_height));
	}
}