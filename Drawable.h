#ifndef SNENGINE_DRAWABLE_H
#define SNENGINE_DRAWABLE_H

namespace se
{
	class Drawable
	{
		friend class Window;

	private:
		virtual void draw(unsigned int shaderID, float windowW, float windowH) = 0;
	};
}

#endif