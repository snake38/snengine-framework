#ifndef SNENGINE_COLOR_H
#define SNENGINE_COLOR_H

namespace se
{
	class Color
	{
	public:
		/**
		 * \brief Sets default color (255, 255, 255, 255)
		 */
		Color();

		/**
		 * \brief Sets color to given parametrs
		 *
		 * \param red Red color [0-255]
		 * \param green Green color [0-255]
		 * \param blue Blue color [0-255]
		 */
		Color(int red, int green, int blue);

		/**
		 * \brief Sets color to given parametrs
		 *
		 * \param red Red color [0-255]
		 * \param green Green color [0-255]
		 * \param blue Blue color [0-255]
		 * \param alpha Alpha [0-255]
		 */
		Color(int red, int green, int blue, int alpha);

		/**
		 * \brief Sets default color with given alpha
		 *
		 * \param alpha Alpha [0-255]
		 */
		Color(int alpha);
		
		int r, g, b, a;
	};
}

#endif