#include "Vec2.h"

namespace se
{
	template<typename T>
	Vec2<T>::Vec2() : x(T()), y(T())
	{
	}

	template<typename T>
	Vec2<T>::Vec2(T newX, T newY) : x(newX), y(newY)
	{
	}

	template class Vec2<int>;
	template class Vec2<float>;
	template class Vec2<double>;
}