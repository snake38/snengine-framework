#include "Timer.h"

namespace se
{
	Timer::Timer() : m_secondsElapsed(0.0), m_firstTimeClock(glfwGetTime())
	{
	}

	double Timer::GetTime()
	{
		m_secondsElapsed = glfwGetTime() - m_firstTimeClock;
		return m_secondsElapsed;
	}

	double Timer::Restart()
	{
		GetTime();
		m_firstTimeClock = glfwGetTime();
		return m_secondsElapsed;
	}
}