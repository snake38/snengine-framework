#include "AudioManager.h"

namespace se
{
	void AudioManager::initialize(int width, int height)
	{
		engine = irrklang::createIrrKlangDevice();
		engine->setListenerPosition(irrklang::vec3df(width / 2, height / 2, 0.f), irrklang::vec3df(0.f, 0.f, 0.f));
	}

	void AudioManager::finalize()
	{
		engine->drop();
	}

	void AudioManager::AddAudio(std::string name, std::string path, bool sound2d, Vec2f position)
	{
		Audio* a = new Audio;
		a->name = name;
		a->path = path;

		a->sndSource = engine->addSoundSourceFromFile(path.c_str(), irrklang::ESM_AUTO_DETECT, true);
		
		if(sound2d)
			a->snd = engine->play3D(a->sndSource, irrklang::vec3df(position.x, position.y, 0.f), false, true, true);
		else
			a->snd = engine->play2D(a->sndSource, false, true, true);

		m_audios.push_back(a);
	}

	void AudioManager::UnloadAudio(std::string name)
	{
		for(unsigned int i = 0; i < m_audios.size(); i++)
		{
			if(m_audios[i]->name == name)
			{
				m_audios[i]->sndSource->drop();
				m_audios[i]->snd->drop();

				delete m_audios[i];
				m_audios.erase(m_audios.begin() + i);

				break;
			}
		}
	}

	void AudioManager::Play(std::string name, bool loop)
	{
		for(unsigned int i = 0; i < m_audios.size(); i++)
		{
			if(m_audios[i]->name == name)
			{
				engine->play2D(m_audios[i]->sndSource, loop);
				break;
			}
		}
	}

	void AudioManager::Play2D(std::string name, bool loop, Vec2f position)
	{
		for(unsigned int i = 0; i < m_audios.size(); i++)
		{
			if(m_audios[i]->name == name)
			{
				engine->play3D(m_audios[i]->sndSource, irrklang::vec3df(position.x, position.y, 0.f), loop);
				break;
			}
		}
	}

	void AudioManager::Stop(std::string name)
	{
		for(unsigned int i = 0; i < m_audios.size(); i++)
		{
			if(m_audios[i]->name == name)
			{
				m_audios[i]->snd->stop();
				break;
			}
		}
	}

	void AudioManager::SetListenerPosition(Vec2f position)
	{
		engine->setListenerPosition(irrklang::vec3df(position.x, position.y, 0.f), irrklang::vec3df(0.f, 0.f, 0.f));
	}
}