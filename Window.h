#ifndef SNENGINE_WINDOW_H
#define SNENGINE_WINDOW_H

#include "GL/glew.h"
#include "GLFW/glfw3.h"

#include "Input.h"
#include "Vec2.h"
#include "Drawable.h"
#include "AudioManager.h"
#include "Color.h"

#include <string>

namespace se
{

	class Window
	{
	private:
		GLFWwindow* m_window;
		unsigned int m_shaderSprite;
		float m_width, m_height;
	public:
		/**
		 * \brief Create window
		 *
		 * \param width Width of the window
		 * \param height Height of the window
		 * \param fullscreen True - fullscreen
		 */
		Window(int width, int height, bool fullscreen = false);

		/**
		 * \brief Destroy window and free memory
		 */
		virtual ~Window();

		/**
		 * \brief Set new title for this window
		 */
		void SetTitle(std::string title);

		/**
		 * \brief Get window size
		 *
		 * \return Window size
		 */
		Vec2i Size();

		/**
		 * \brief Checks if window should close
		 *
		 * This determines if window is opened or if close was called
		 *
		 * \return True if should close
		 *
		 * \see Close()
		 */
		bool ShouldClose();

		/**
		 * \brief Sends to window request to close window
		 *
		 * \see ShouldClose()
		 */
		void Close();

		/**
		 * \brief Clears screen and fills with passed color
		 *
		 * \param color Color class
		 *
		 */
		void Clear(Color color);

		/**
		 * \brief Calls engine to finish rendering
		 */
		void Render();

		/**
		 * \brief Draw object on the screen
		 */
		void Draw(Drawable* object);

		/**
		 * \brief Deletes window objects and internals
		 *
		 * \see ShouldClose()
		 * \see Close()
		 */
		void Shutdown();

		/**
		 * \brief Returns state of key
		 *
		 * \param key Key from Keyboard::KEYS enum
		 *
		 * \return True if pressed
		 */
		bool GetKey(Input::KEYS key);

		/**
		 * \brief Returns position of mouse in a window
		 * 
		 * \return Position of mouse in Vec2i
		 */
		Vec2i GetMousePos();

		/**
		 * \brief Is mouse button clicked?
		 *
		 * \return True if is pressed
		 */
		bool GetMouseButton(Input::MOUSE button);
	};
}

#endif