#include "Image.h"

namespace se
{
	Image::Image(std::string path) : m_Width(0), m_Height(0)
	{
		/*unsigned char header[54];
		unsigned int dataPos;
		unsigned int imageSize;
		
		unsigned char* bits;

		FILE* f = NULL;
		fopen_s(&f, path.c_str(), "rb");
		fread(header, 1, 54, f);

		dataPos = *(int*)&(header[0x0A]);
		imageSize = *(int*)&(header[0x22]);
		m_Width = *(int*)&(header[0x12]);
		m_Height = *(int*)&(header[0x16]);

		if(imageSize == 0) imageSize = m_Width * m_Height * 3;
		if(dataPos == 0) dataPos = 54;

		bits = new unsigned char[imageSize];

		fread(bits, 1, imageSize, f);

		fclose(f);*/
		
		FREE_IMAGE_FORMAT fif = FIF_UNKNOWN;
		FIBITMAP* dib(0);
		BYTE* bits(0);

		fif = FreeImage_GetFileType(path.c_str(), 0);
		if(fif == FIF_UNKNOWN)
			fif = FreeImage_GetFIFFromFilename(path.c_str());
		if(fif == FIF_UNKNOWN)
			return;

		if(FreeImage_FIFSupportsReading(fif))
			dib = FreeImage_Load(fif, path.c_str());

		if(!dib)
			return;

		bits = FreeImage_GetBits(dib);
		m_Width = FreeImage_GetWidth(dib);
		m_Height = FreeImage_GetHeight(dib);
		
		if(bits == 0 || m_Width == 0 || m_Height == 0)
			return;
			
		glGenTextures(1, &m_texID);

		glBindTexture(GL_TEXTURE_2D, m_texID);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_Width, m_Height, 0, GL_BGRA, GL_UNSIGNED_BYTE, bits);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		

		FreeImage_Unload(dib);
	}

	Vec2i Image::Size()
	{
		return Vec2<int>(m_Width, m_Height);
	}
}