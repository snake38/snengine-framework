#ifndef SNENGINE_IMAGE_H
#define SNENGINE_IMAGE_H

#include <string>
#include "Vec2.h"

#include "GL/glew.h"
#include <FreeImage.h>

namespace se
{
	class Image
	{
		friend class Sprite;

	private:
		int m_Width, m_Height;

		unsigned int m_texID;
	public:	

		/**
		 * \brief Load image into memory. Only BMP are supported for now, no error checking!
		 */
		Image(std::string path);

		/**
		 * \brief Returns size of the image in pixels
		 *
		 * \return Image size
		 */
		Vec2i Size();
	};
}

#endif