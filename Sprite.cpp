#include "Sprite.h"

namespace se
{
	Sprite::Sprite(Image* image) : 
		m_Image(image), m_Pos(0, 0), m_Origin(0, 0), m_Rotation(0), m_Scale(1.f, 1.f),
		m_imgPos(0, 0), m_imgSize(image->Size()),
		m_framesCount(0), m_framesX(0), m_framesY(0), m_actualFrame(0), m_animDirection(1),
		m_animPlay(false), m_animFPS(0), m_animStartFrame(0), m_animEndFrame(0), m_animStartOld(0)
	{
		/*glGenVertexArrays(1, &m_vertexArrayID);
		glBindVertexArray(m_vertexArrayID);

		Vec2i imgSize = image->Size();
		float width = float(m_imgSize.x);
		float height = float(m_imgSize.y);

		GLfloat vertexBufferData[] = {
			-width * 0.5f, -height * 0.5f, 0.f,	// triangle1
			-width * 0.5f,  height * 0.5f, 0.f,
			 width * 0.5f,  height * 0.5f, 0.f,

			-width * 0.5f, -height * 0.5f, 0.f,	// triangle2
			 width * 0.5f, -height * 0.5f, 0.f,
			 width * 0.5f,  height * 0.5f, 0.f
		};

		GLfloat vertexUVData[] = {
			 0.f, 1.f,							// tri1 UV
			 0.f, 0.f,
			 1.f, 0.f,

			 0.f, 1.f,							// tri2 UV
			 1.f, 1.f,
			 1.f, 0.f
		};

		glGenBuffers(1, &m_vertexBufferID);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBufferData), vertexBufferData, GL_STATIC_DRAW);

		glGenBuffers(1, &m_vertexUVID);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexUVID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexUVData), vertexUVData, GL_STATIC_DRAW);*/

		makeSprite();
	}

	Sprite::Sprite(Image* image, Vec2f position, Vec2i size) : 
		m_Image(image), m_Pos(0, 0), m_Origin(0, 0), m_Rotation(0), m_Scale(1.f, 1.f),
		m_imgPos(position), m_imgSize(size),
		m_framesCount(0), m_framesX(0), m_framesY(0), m_actualFrame(0), m_animDirection(1),
		m_animPlay(false), m_animFPS(0), m_animStartFrame(0), m_animEndFrame(0), m_animStartOld(0)
	{
		/*glGenVertexArrays(1, &m_vertexArrayID);
		glBindVertexArray(m_vertexArrayID);

		float width = float(m_imgSize.x);
		float height = float(m_imgSize.y);

		GLfloat vertexBufferData[] = {
			-width * 0.5f, -height * 0.5f, 0.f,	// triangle1
			-width * 0.5f,  height * 0.5f, 0.f,
			 width * 0.5f,  height * 0.5f, 0.f,

			-width * 0.5f, -height * 0.5f, 0.f,	// triangle2
			 width * 0.5f, -height * 0.5f, 0.f,
			 width * 0.5f,  height * 0.5f, 0.f,
		};

		GLfloat vertexUVData[] = {
			// tri1
			m_imgPos.x / m_Image->Size().x,					(m_Image->Size().y - m_imgPos.y) / m_Image->Size().y,
			m_imgPos.x / m_Image->Size().x,					(m_Image->Size().y - (m_imgPos.y + m_imgSize.y)) / m_Image->Size().y,
			(m_imgPos.x + m_imgSize.x) / m_Image->Size().x,	(m_Image->Size().y - (m_imgPos.y + m_imgSize.y)) / m_Image->Size().y,

			//tri2
			m_imgPos.x / m_Image->Size().x,					(m_Image->Size().y - m_imgPos.y) / m_Image->Size().y,
			(m_imgPos.x + m_imgSize.x) / m_Image->Size().x,	(m_Image->Size().y - m_imgPos.y) / m_Image->Size().y,
			(m_imgPos.x + m_imgSize.x) / m_Image->Size().x,	(m_Image->Size().y - (m_imgPos.y + m_imgSize.y)) / m_Image->Size().y
		};

		glGenBuffers(1, &m_vertexBufferID);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBufferData), vertexBufferData, GL_STATIC_DRAW);

		glGenBuffers(1, &m_vertexUVID);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexUVID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexUVData), vertexUVData, GL_STATIC_DRAW);*/

		makeSprite();
	}

	Sprite::Sprite(Image* image, Vec2i size) : 
		m_Image(image), m_Pos(0, 0), m_Origin(0, 0), m_Rotation(0), m_Scale(1.f, 1.f),
		m_imgPos(0, 0), m_imgSize(size),
		m_framesCount(0), m_framesX(0), m_framesY(0), m_actualFrame(0), m_animDirection(1),
		m_animPlay(false), m_animFPS(0), m_animStartFrame(0), m_animEndFrame(0), m_animStartOld(0)
	{
		/*glGenVertexArrays(1, &m_vertexArrayID);
		glBindVertexArray(m_vertexArrayID);

		float width = float(m_imgSize.x);
		float height = float(m_imgSize.y);

		GLfloat vertexBufferData[] = {
			-width * 0.5f, -height * 0.5f, 0.f,	// triangle1
			-width * 0.5f,  height * 0.5f, 0.f,
			 width * 0.5f,  height * 0.5f, 0.f,

			-width * 0.5f, -height * 0.5f, 0.f,	// triangle2
			 width * 0.5f, -height * 0.5f, 0.f,
			 width * 0.5f,  height * 0.5f, 0.f,
		};

		GLfloat vertexUVData[] = {
			// tri1
			m_imgPos.x / m_Image->Size().x,					(m_Image->Size().y - m_imgPos.y) / m_Image->Size().y,
			m_imgPos.x / m_Image->Size().x,					(m_Image->Size().y - (m_imgPos.y + m_imgSize.y)) / m_Image->Size().y,
			(m_imgPos.x + m_imgSize.x) / m_Image->Size().x,	(m_Image->Size().y - (m_imgPos.y + m_imgSize.y)) / m_Image->Size().y,

			//tri2
			m_imgPos.x / m_Image->Size().x,					(m_Image->Size().y - m_imgPos.y) / m_Image->Size().y,
			(m_imgPos.x + m_imgSize.x) / m_Image->Size().x,	(m_Image->Size().y - m_imgPos.y) / m_Image->Size().y,
			(m_imgPos.x + m_imgSize.x) / m_Image->Size().x,	(m_Image->Size().y - (m_imgPos.y + m_imgSize.y)) / m_Image->Size().y
		};

		glGenBuffers(1, &m_vertexBufferID);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBufferData), vertexBufferData, GL_STATIC_DRAW);

		glGenBuffers(1, &m_vertexUVID);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexUVID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexUVData), vertexUVData, GL_STATIC_DRAW);*/

		makeSprite();
	}

	void Sprite::makeSprite()
	{
		m_framesX = m_Image->Size().x / m_imgSize.x;
		m_framesY = m_Image->Size().y / m_imgSize.y;
		m_framesCount = m_framesX * m_framesY;

		glGenVertexArrays(1, &m_vertexArrayID);
		glBindVertexArray(m_vertexArrayID);

		float width = float(m_imgSize.x);
		float height = float(m_imgSize.y);

		GLfloat vertexBufferData[] = {
			-width * 0.5f, -height * 0.5f, 0.f,	// triangle1
			-width * 0.5f,  height * 0.5f, 0.f,
			 width * 0.5f,  height * 0.5f, 0.f,

			-width * 0.5f, -height * 0.5f, 0.f,	// triangle2
			 width * 0.5f, -height * 0.5f, 0.f,
			 width * 0.5f,  height * 0.5f, 0.f,
		};

		GLfloat vertexUVData[] = {
			// tri1
			m_imgPos.x / m_Image->Size().x,					(m_Image->Size().y - m_imgPos.y) / m_Image->Size().y,
			m_imgPos.x / m_Image->Size().x,					(m_Image->Size().y - (m_imgPos.y + m_imgSize.y)) / m_Image->Size().y,
			(m_imgPos.x + m_imgSize.x) / m_Image->Size().x,	(m_Image->Size().y - (m_imgPos.y + m_imgSize.y)) / m_Image->Size().y,

			//tri2
			m_imgPos.x / m_Image->Size().x,					(m_Image->Size().y - m_imgPos.y) / m_Image->Size().y,
			(m_imgPos.x + m_imgSize.x) / m_Image->Size().x,	(m_Image->Size().y - m_imgPos.y) / m_Image->Size().y,
			(m_imgPos.x + m_imgSize.x) / m_Image->Size().x,	(m_Image->Size().y - (m_imgPos.y + m_imgSize.y)) / m_Image->Size().y
		};

		float r = color.r / 255.f; float g = color.g / 255.f; float b = color.b / 255.f; float a = color.a / 255.f;
		GLfloat blendingColorData[] = {
			r, g, b, a,
			r, g, b, a,
			r, g, b, a,
			r, g, b, a,
			r, g, b, a,
			r, g, b, a
		};

		glGenBuffers(1, &m_vertexBufferID);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBufferData), vertexBufferData, GL_STATIC_DRAW);

		glGenBuffers(1, &m_vertexUVID);
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexUVID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexUVData), vertexUVData, GL_STATIC_DRAW);

		glGenBuffers(1, &m_blendingColorID);
		glBindBuffer(GL_ARRAY_BUFFER, m_blendingColorID);
		glBufferData(GL_ARRAY_BUFFER, sizeof(blendingColorData), blendingColorData, GL_STATIC_DRAW);
	}

	void Sprite::SetTexturePosition(Vec2f position)
	{
		m_imgPos = position;

		GLfloat vertexUVData[] = {
			// tri1
			m_imgPos.x / m_Image->Size().x,					(m_Image->Size().y - m_imgPos.y) / m_Image->Size().y,
			m_imgPos.x / m_Image->Size().x,					(m_Image->Size().y - (m_imgPos.y + m_imgSize.y)) / m_Image->Size().y,
			(m_imgPos.x + m_imgSize.x) / m_Image->Size().x,	(m_Image->Size().y - (m_imgPos.y + m_imgSize.y)) / m_Image->Size().y,

			//tri2
			m_imgPos.x / m_Image->Size().x,					(m_Image->Size().y - m_imgPos.y) / m_Image->Size().y,
			(m_imgPos.x + m_imgSize.x) / m_Image->Size().x,	(m_Image->Size().y - m_imgPos.y) / m_Image->Size().y,
			(m_imgPos.x + m_imgSize.x) / m_Image->Size().x,	(m_Image->Size().y - (m_imgPos.y + m_imgSize.y)) / m_Image->Size().y
		};

		glBindBuffer(GL_ARRAY_BUFFER, m_vertexUVID);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertexUVData), vertexUVData);
	}

	void Sprite::SetColor(Color c)
	{
		color = c;

		float r = color.r / 255.f; float g = color.g / 255.f; float b = color.b / 255.f; float a = color.a / 255.f;
		GLfloat blendingColorData[] = {
			r, g, b, a,
			r, g, b, a,
			r, g, b, a,
			r, g, b, a,
			r, g, b, a,
			r, g, b, a
		};

		glBindBuffer(GL_ARRAY_BUFFER, m_blendingColorID);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(blendingColorData), blendingColorData);
	}

	Color Sprite::GetColor()
	{
		return color;
	}

	void Sprite::draw(unsigned int shaderID, float windowW, float windowH)
	{
		if(m_animPlay)
		{
			double dt = m_animTimer.GetTime();

			if(float(dt) >= m_animFPS || m_animStartFrame != m_animStartOld)
			{

				if(m_actualFrame != m_animEndFrame + 1 || m_animLoop)
				{
					if(m_animLoop && m_actualFrame > m_animEndFrame)
						m_actualFrame = m_animStartFrame;

					// change frame
					// calculate frame position and set it
					SetTexturePosition(Vec2<float>(float((m_actualFrame % m_framesX) * m_imgSize.x), float((m_actualFrame / m_framesX) * m_imgSize.y)));

					m_actualFrame += m_animDirection;
				}
				else
				{
					m_animPlay = false;
				}

				m_animTimer.Restart();
			}
		}

		float width  = float(m_imgSize.x * m_Scale.x);
		float height = float(m_imgSize.y * m_Scale.y);

		glm::mat4 projectionMatrix = glm::ortho(0.0f, windowW, windowH, 0.0f);
		glm::mat4 modelMatrix = glm::mat4(1.f);
		
		modelMatrix = glm::translate(modelMatrix, glm::vec3(m_Pos.x + width * 0.5f - m_Origin.x, m_Pos.y + height * 0.5f - m_Origin.y, 1.0f));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(m_Scale.x, m_Scale.y, 1.f));
		modelMatrix = glm::rotate(modelMatrix, m_Rotation, glm::vec3(0.f, 0.f, 1.0f));

		glm::mat4 MP = projectionMatrix * modelMatrix;

		glUseProgram(shaderID);

		GLuint matrixID = glGetUniformLocation(shaderID, "MP");
		glUniformMatrix4fv(matrixID, 1, GL_FALSE, &MP[0][0]);

		GLuint vPos = glGetAttribLocation(shaderID, "vertexPos");
		GLuint uvPos = glGetAttribLocation(shaderID, "vertexUV");
		GLuint texPos = glGetUniformLocation(shaderID, "textureSampler");
		GLuint blendingColorPos = glGetAttribLocation(shaderID, "blendingColor");

		glEnableVertexAttribArray(vPos);
		glEnableVertexAttribArray(uvPos);
		glEnableVertexAttribArray(blendingColorPos);

		glBindBuffer(GL_ARRAY_BUFFER, m_vertexBufferID);
		glVertexAttribPointer(
			vPos,
			3,
			GL_FLOAT,
			GL_FALSE,
			0,
			(void*)0
		);
		
		glBindBuffer(GL_ARRAY_BUFFER, m_vertexUVID);
		glVertexAttribPointer(
			uvPos,
			2,
			GL_FLOAT,
			GL_FALSE,
			0,
			(void*)0
		);

		glBindBuffer(GL_ARRAY_BUFFER, m_blendingColorID);
		glVertexAttribPointer(
			blendingColorPos,
			4,
			GL_FLOAT,
			GL_FALSE,
			0,
			(void*)0
		);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_Image->m_texID);
		glUniform1i(texPos, 0);

		glDrawArrays(GL_TRIANGLES, 0, 6);

		glDisableVertexAttribArray(blendingColorPos);
		glDisableVertexAttribArray(uvPos);
		glDisableVertexAttribArray(vPos);
	}

	void Sprite::SetPosition(Vec2f vector)
	{
		m_Pos = vector;
	}

	void Sprite::SetPosition(float x, float y)
	{
		m_Pos.x = x;
		m_Pos.y = y;
	}

	void Sprite::SetOrigin(Vec2f vector)
	{
		m_Origin = vector;
	}

	void Sprite::SetOrigin(float x, float y)
	{
		m_Origin.x = x;
		m_Origin.y = y;
	}

	void Sprite::Move(Vec2f vector)
	{
		m_Pos.x += vector.x;
		m_Pos.y += vector.y;
	}

	void Sprite::Move(float x, float y)
	{
		m_Pos.x += x;
		m_Pos.y += y;
	}

	Vec2f Sprite::GetPosition()
	{
		return m_Pos;
	}

	void Sprite::SetRotation(float degree)
	{
		m_Rotation = degree;
	}

	float Sprite::GetRotation()
	{
		return m_Rotation;
	}

	void Sprite::SetAnimationFrames(int start, int end)
	{
		m_animStartOld = m_animStartFrame;

		m_animStartFrame = start;
		m_animEndFrame = end;

		if(m_animStartFrame < 0) m_animStartFrame = 0;
		if(m_animEndFrame >= m_framesCount) m_animEndFrame = m_framesCount - 1;

		if(m_actualFrame < m_animStartFrame || m_actualFrame > m_animEndFrame)
			m_actualFrame = m_animStartFrame;
	}

	void Sprite::SetAnimationFrames(Vec2i frames)
	{
		m_animStartOld = m_animStartFrame;

		m_animStartFrame = frames.x;
		m_animEndFrame = frames.y;

		if(m_animStartFrame < 0) m_animStartFrame = 0;
		if(m_animEndFrame >= m_framesCount) m_animEndFrame = m_framesCount - 1;

		if(m_animEndFrame > m_animStartFrame)
			m_animDirection = 1;
		else
			m_animDirection = -1;
	}

	void Sprite::SetAnimationSpeed(float fps)
	{
		m_animFPS = fps;
	}

	float Sprite::GetAnimationSpeed()
	{
		return m_animFPS;
	}

	void Sprite::SetAnimationLoop(bool loop)
	{
		m_animLoop = loop;
	}

	void Sprite::AnimationStart()
	{
		m_animPlay = true;
		m_actualFrame = m_animStartFrame;
		m_animTimer.Restart();
	}

	void Sprite::AnimationStop()
	{
		m_animPlay = false;
	}

	bool Sprite::IsLastFrame()
	{
		if(m_actualFrame == m_animEndFrame)
			return true;
		else
			return false;
	}

	void Sprite::SetFrame(unsigned int frame)
	{
		if(frame < 0) frame = 0;
		if(frame > m_framesCount - 1) frame = m_framesCount - 1;

		m_actualFrame = frame;

		SetTexturePosition(Vec2<float>(float((m_actualFrame % m_framesX) * m_imgSize.x), float((m_actualFrame / m_framesX) * m_imgSize.y)));
	}

	Vec2i Sprite::Size()
	{
		Vec2i size;
		size.x = int(m_imgSize.x * m_Scale.x);
		size.y = int(m_imgSize.y * m_Scale.y);
		return size;
	}

	void Sprite::SetScale(float x, float y)
	{
		m_Scale.x = x;
		m_Scale.y = y;
	}

	void Sprite::SetScale(Vec2f scale)
	{
		m_Scale = scale;
	}

	Vec2f Sprite::GetScale()
	{
		return m_Scale;
	}


}