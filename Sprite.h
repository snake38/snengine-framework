#ifndef SNENGINE_SPRITE_H
#define SNENGINE_SPRITE_H

#include "Image.h"
#include "Vec2.h"
#include "Drawable.h"
#include "Timer.h"
#include "Color.h"

#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

namespace se
{
	class Sprite : public Drawable
	{
	private:
		Vec2f m_Pos;
		Vec2f m_Origin;
		Vec2f m_imgPos;
		Vec2i m_imgSize;
		float m_Rotation;
		Vec2f m_Scale;
		Image* m_Image;

		Color color;

		unsigned int m_framesX, m_framesY;
		unsigned int m_framesCount, m_actualFrame;
		bool m_animPlay, m_animLoop;
		unsigned int m_animStartFrame, m_animEndFrame, m_animStartOld;
		int m_animDirection;
		float m_animFPS;

		Timer m_animTimer;

		unsigned int m_vertexArrayID;
		unsigned int m_vertexBufferID;
		unsigned int m_vertexUVID;
		unsigned int m_blendingColorID;

		void makeSprite();
		void draw(unsigned int shaderID, float windowW, float windowH);
	public:

		/**
		 * \brief Creates sprite object with passed image
		 */
		Sprite(Image* image);

		/**
		 * \brief Creates sprite object with passed image and given size
		 *
		 * \param position Pixel position on texture
		 * \param size Size of sprite
		 */
		Sprite(Image* image, Vec2f position, Vec2i size);

		/**
		 * \brief Creates sprite object with given size, using passed image
		 *
		 * \param size Size of sprite
		 */
		Sprite(Image* image, Vec2i size);

		/**
		 * \brief Set texture position
		 *
		 * \param position Position in pixels on texture
		 */
		void SetTexturePosition(Vec2f position);

		/**
		 * \brief Set position of sprite
		 */
		void SetPosition(Vec2f vector);

		/**
		 * \brief Set position of sprite
		 */
		void SetPosition(float x, float y);

		/**
		 * \brief Set origin of sprite
		 */
		void SetOrigin(Vec2f vector);

		/**
		 * \brief Set origin of sprite
		 */
		void SetOrigin(float x, float y);

		/**
		 * \brief Move sprite by given pixels
		 */
		void Move(Vec2f vector);

		/**
		 * \brief Move sprite by given pixels
		 */
		void Move(float x, float y);

		/**
		 * \brief Returns actual position of the sprite
		 *
		 * \return Position
		 */
		Vec2f GetPosition();

		/**
		 * \brief Sets rotation for sprite in degrees
		 */
		void SetRotation(float degree);

		/**
		 * \brief Get rotation of sprite
		 *
		 * \return Rotation in degrees
		 */
		float GetRotation();

		/**
		 * \brief Sets animation frames
		 *
		 * \param start Start frame
		 * \param end End frame
		 */
		void SetAnimationFrames(int start, int end);

		/**
		 * \brief Sets animation frames
		 *
		 * \param frames Start and end frame
		 */
		void SetAnimationFrames(Vec2i frames);

		/**
		 * \brief Sets animation speed
		 *
		 * \param fps Animation speed in frames per second
		 */
		void SetAnimationSpeed(float fps);

		/**
		 * \brief Gets actual animation speed
		 *
		 * \return Animatoin speed in frames per second
		 */
		float GetAnimationSpeed();

		/**
		 * \brief Sets animation loop
		 *
		 * \param loop True if play in loop
		 */
		void SetAnimationLoop(bool loop);

		/**
		 * \brief Start animations
		 */
		void AnimationStart();

		/**
		 * \brief Stops animations
		 */
		void AnimationStop();

		/**
		 * \brief Checks if actual frame is last frame
		 *
		 * If animation is looped it will return true when actual frame is equal to end frame
		 *
		 * \return True if it is
		 */
		bool IsLastFrame();

		/**
		 * \brief Sets frame for render
		 *
		 * Doesn't matter if animation is played or not. You can use it as sprite selector
		 *
		 * \param frame Frame number
		 */
		void SetFrame(unsigned int frame);

		/**
		 * \brief Get size of sprite
		 */
		Vec2i Size();

		/**
		 * \brief Set color of sprite
		 *
		 * \param color Color class
		 */
		void SetColor(Color color);

		/**
		 * \brief Returns actual color
		 */
		Color GetColor();

		/**
		 * \brief Set scale for sprite
		 *
		 * \param x Scale in x-axis
		 * \param y Scale in y-axis
		 */
		void SetScale(float x, float y);

		/**
		 * \brief Set scale for sprite
		 *
		 * \param scale Vector scale
		 */
		void SetScale(Vec2f scale);

		/**
		 * \brief Get sprite scale
		 *
		 * \return Scale of sprite
		 */
		Vec2f GetScale();
	};
}

#endif