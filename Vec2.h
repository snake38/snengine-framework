#ifndef SNENGINE_VEC2_H
#define SNENGINE_VEC2_H

namespace se
{
	template <typename T>
	class Vec2
	{
	private:
		
	public:
		T x, y;

		Vec2();
		Vec2(T x, T y);
	};

	typedef Vec2<int> Vec2i;
	typedef Vec2<float> Vec2f;
	typedef Vec2<double> Vec2d;
}

#endif