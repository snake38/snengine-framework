#ifndef SNENGINE_AUDIOMANAGER_H
#define SNENGINE_AUDIOMANAGER_H

#include "irrKlang.h"
#include "Vec2.h"
#include <string>
#include <vector>

namespace se
{
#define AudioMgr AudioManager::getInstance()

	class AudioManager
	{
		friend class Window;

	private:
		AudioManager() {}
		AudioManager(AudioManager const&);
		void operator=(AudioManager const&);

		void initialize(int width, int height);
		void finalize();

		irrklang::ISoundEngine* engine;

		struct Audio {
			std::string name;
			std::string path;
			irrklang::ISoundSource* sndSource;
			irrklang::ISound* snd;
		};

		std::vector<Audio*> m_audios;

	public:
		/**
		 * \brief Return instance of AudioManager
		 */
		static AudioManager& getInstance() {
			static AudioManager instance;
			return instance;
		}

		/**
		 * \brief Load audio into memory
		 *
		 * \param name Unique name of your sound, can be anything
		 * \param path Path to the sound
		 * \param sound2d True if sound should be played in 2d space
		 * \param position Position of sound in space
		 *
		 * \see SetListenerPosition()
		 */
		void AddAudio(std::string name, std::string path, bool sound2d = false, Vec2f position = Vec2<float>(0, 0));

		/**
		 * \brief Unload audio from memory
		 *
		 * \param name Name of your sound
		 */
		void UnloadAudio(std::string name);

		/**
		 * \brief Play audio
		 */
		void Play(std::string name, bool loop = false);

		/**
		 * \brief Play audio with position in space
		 *
		 * \param name Your name defined with AddAudio method
		 * \param loop True if sound should be looped
		 * \param position Position of sound in space
		 *
		 * \see SetListenerPosition()
		 */
		void Play2D(std::string name, bool loop = false, Vec2f position = Vec2<float>(0, 0));

		/**
		 * \brief Stops sound
		 */
		void Stop(std::string name);
	
		/**
		 * \brief Sets position of listener, default is center of screen
		 *
		 * \param position Position in space
		 */
		void SetListenerPosition(Vec2f position);
	};
}

#endif